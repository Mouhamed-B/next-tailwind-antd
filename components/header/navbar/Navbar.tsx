import { Button } from '../../../components'
interface navItemsIterface {
  name: string
  link: string
}

const Navbar = () => {
  const navItems: navItemsIterface[] = [
    { name: 'Home', link: '' },
    { name: 'About', link: '' },
    { name: 'Departments', link: '' },
    { name: 'Forum', link: '' },
    { name: 'Blog', link: '' },
    { name: 'Contact', link: '' },
  ]

  return (
    <nav className="flex items-center justify-between py-1 px-10 shadow-md ">
      <div className="flex items-center">
        <img src="icons/Vector.svg" alt="logo" />
        <h4 className="mb-0 ml-2 font-bold tracking-wider">Dr Fanel</h4>
      </div>
      <div className="flex space-x-6">
        {navItems.map((item: navItemsIterface) => (
          <a href={item.link} key={item.name} className="hover:text-">
            <h6>{item.name}</h6>
          </a>
        ))}
      </div>
      <Button />
    </nav>
  )
}

export default Navbar
