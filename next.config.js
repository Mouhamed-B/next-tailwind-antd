const withLess = require('next-with-less');

/** @type {import('next').NextConfig} */
module.exports = withLess({
lessLoaderOptions: {
    lessOptions: {
      javascriptEnabled: true,
      modifyVars: {
        'primary-color':' #3ca3b0', // primary color for all components

        'link-color':' #2522ba', // link color
        'info-error':' #838383',
        'success-color':' #27ae60', // success state color
        'warning-color':' #e2b93b', // warning state color
        'error-color':' #eb5757', // error state color

        'font-size-base':' 14px', // major text font size
        'heading-color':' #02414b', // heading text color
        'text-color':' #292930', // major text color
        'text-color-secondary':' #999fae', // secondary text color
        'disabled-color':' rgba(0, 0, 0, 0.25)', // disable state color

        'border-radius-base':' 12px', // major border radius
        'border-color-base':' currentColor', // major border color

        'box-shadow-base':' 0 2px 8px rgba(0, 0, 0, 0.15)', // major shadow for layers
      }, // make your antd custom effective
      localIdentName: '[path]___[local]___[hash:base64:5]',
    },
  },
})